package app

import (
	"context"
	"testing"
)

func TestComposeGreeting(t *testing.T) {
	type args struct {
		ctx  context.Context
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Happy Path",
			args: args{
				ctx:  context.Background(),
				name: "World",
			},
			want: "Hello World!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ComposeGreeting(tt.args.ctx, tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("ComposeGreeting() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ComposeGreeting() got = %v, want %v", got, tt.want)
			}
		})
	}
}
